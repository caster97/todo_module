<?php


namespace Finoghentov\TodoModule\Controllers;


use Finoghentov\TodoModule\Requests\Table\AddTaskRequest;
use Finoghentov\TodoModule\Requests\Table\ChangeTaskPositionRequest;
use Finoghentov\TodoModule\Requests\Table\DeleteTaskRequest;

class TaskController extends ApiController
{
    /**
     * @param AddTaskRequest $request
     * @return string json
     */
    public function addTask(AddTaskRequest $request){
        return parent::api_addTask($request);
    }

    /**
     * @param DeleteTaskRequest $request
     * @return string json
     */
    public function deleteTask(DeleteTaskRequest $request){
        return parent::api_deleteTask($request);
    }

//    public function changeStatusText(Request $request){
//        $task =  Task::find($request->id);
//        switch($task->status){
//            case 'new':
//                $task->status = 'completed';
//                break;
//            case 'completed':
//                $task->status = 'new';
//                break;
//        }
//        $task->save();
//        return response()->json($task->status);
//    }

    /**
     * @param ChangeTaskPositionRequest $request
     * @return string json
     */
    public function changeTaskPosition(ChangeTaskPositionRequest $request){
        return parent::api_changeTaskPosition($request);
    }
}
