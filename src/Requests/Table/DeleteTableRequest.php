<?php

namespace Finoghentov\TodoModule\Requests\Table;

use Finoghentov\TodoModule\Requests\ApiRequest;

class DeleteTableRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer'
        ];
    }
}
