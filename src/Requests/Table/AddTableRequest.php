<?php

namespace Finoghentov\TodoModule\Requests\Table;

use Finoghentov\TodoModule\Requests\ApiRequest;

class AddTableRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:20'
        ];
    }
}
