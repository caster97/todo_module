<?php

namespace Finoghentov\TodoModule\Requests\Table;

use Finoghentov\TodoModule\Requests\ApiRequest;

class AddTaskRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'task' => 'required|string|min:3',
            'card_id' => 'required|integer'
        ];
    }
}
