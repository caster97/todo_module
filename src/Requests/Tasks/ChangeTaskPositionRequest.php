<?php

namespace Finoghentov\TodoModule\Requests\Table;

use Finoghentov\TodoModule\Requests\ApiRequest;

class ChangeTaskPositionRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'card_id' => 'required|integer',
            'prev_card_id' => 'required|integer',
            'order' => 'required|integer'
        ];
    }
}
