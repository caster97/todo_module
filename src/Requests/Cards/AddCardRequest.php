<?php

namespace Finoghentov\TodoModule\Requests\Cards;

use Finoghentov\TodoModule\Requests\ApiRequest;

class AddCardRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:25',
            'table_id' => 'required|integer'
        ];
    }
}
