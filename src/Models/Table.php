<?php


namespace Finoghentov\TodoModule\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $table = 'todo_tables';

    protected $guarded = [];

    public function cards(){
        return $this->hasMany(Card::class)->order();
    }

    public function scopeOrder($query){
        return $query->orderBy('order', 'asc');
    }

    /**
     * Getting order sorting for new element
     * @return int
     */
    private static function getNewOrder(){
        return self::all()->count();
    }

    /**
     * Sorting tables after moving one of them
     * @param $table_id int
     * @param $order int
     */
    public static function sortTables($table_id, $order){
        $table = Table::findOrFail($table_id);
        $tables = Table::where('id', '!=', $table_id)->orderBy('order', 'asc')->get();

        $tables->splice($order, 0,  [$table]);
        $i = 1;
        foreach($tables as $table){
            $table->order = $i;
            $table->save();
            $i++;
        }
    }

    protected static function boot() {
        parent::boot();
        /**
         * Setting order sort after creating
         */
        self::created(function($table){
            $table->order = self::getNewOrder();
            $table->save();
        });

        /**
         * Before deleting Model , delete relations
         */
        self::deleting(function($table){
            $table->cards()->each(function($card){
                $card->delete();
            });
        });

    }
}
