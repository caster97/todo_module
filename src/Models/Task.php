<?php


namespace Finoghentov\TodoModule\Models;


use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'todo_tasks';

    protected $guarded = [];

    public function cards(){
        return $this->belongsTo(Card::class);
    }

    public function scopeOrder($query){
        return $query->orderBy('order', 'asc');
    }

    /**
     * Sorting elements in card after deleting or moving element to another card
     * @param $card_id
     */
    public static function sortPrevCardTask($card_id){
        $tasks = Task::where('card_id', $card_id)->orderBy('order','asc')->get();
        $i = 1;
        foreach($tasks as $task){
            $task->order = $i;
            $task->save();
            $i++;
        }
    }

    /**
     * Sorting elements in card that we move an element
     * @param $card_id
     */
    public static function sortCurrentCardTasks($data, self $task){

        if($data['prev_card_id'] == $data['prev_card_id']) {
            $tasks = Task::where('order','!=',$data['old_index'])->where('card_id', $data['card_id'])->order()->get();
        }else{
            $tasks = Task::where('card_id', $data['card_id'])->order()->get();
        }
        $tasks->splice($data['order'], 0,  [$task]);
        $i=1;
        foreach($tasks as $item){
            $item->order = $i;
            $item->save();
            $i++;
        }
    }

    /**
     * Getting order sorting for new element
     * @return int
     */
    private static function getNewOrder(){
        return self::all()->count();
    }

    protected static function boot() {
        parent::boot();
        /**
         * Setting order sort after creating
         */
        self::created(function($task){
            $task->order = self::getNewOrder();
            $task->save();
        });

        /**
         * After deleting Model , sorting element card
         */
        self::deleted(function($task){
            self::sortPrevCardTask($task->card_id);
        });
    }
}
