# Todo Component

Todo Component for your Laravel Nova Admin

## About

With this Todo Component you will have your own todo tool in your Laravel Nova Admin Panel. You can create tables, cards and tasks. Also supports draggable.

### Prerequisites

This tool requires:

```
"laravel/nova": ">=2.9"
```

### Installing

Download via composer

```
composer require finoghentov/todo-module
```

Run migrations

```
php artisan migrate
```

Add Tool to your NovaServiceProvider
```
    //NovaServiceProvider.php
    
    public function tools()
    {
        return [
            new TodoModule
        ];
    }
```

![](/examples/example.gif)

